﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System;
using System.Linq;
using SampleSupport;
using Task.Data;

// Version Mad01

namespace SampleQueries
{
	[Title("LINQ Module")]
	[Prefix("Linq")]
	public class LinqSamples : SampleHarness
	{
		private static readonly DataSource DataSource = new DataSource();


		[Category("Restriction Operators")]
		[Title("Where - Task 1")]
		[Description(
			"Выдайте список всех клиентов, чей суммарный оборот (сумма всех заказов) превосходит некоторую величину X. Продемонстрируйте выполнение запроса с различными X (подумайте, можно ли обойтись без копирования запроса несколько раз)")]
		public void Linq1()
		{
			decimal[] X = {1000, 24926};

			foreach (var price in X)
			{
				var goodCustomers = DataSource.Customers.Where(customer => customer.Orders?.Sum(order => order.Total) > price);
				foreach (var customer in goodCustomers)
				{
					ObjectDumper.Write(customer);
				}

				ObjectDumper.Write("----------------------------------------------------------------------------");
			}
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 2")]
		[Description(
			"Для каждого клиента составьте список поставщиков, находящихся в той же стране и том же городе. Сделайте задания с использованием группировки и без.")]
		public void Linq2()
		{
			var suppliersForCustomerWithoutGroupBy = DataSource.Customers
				.Select(customer => new
				{
					NewCustomer = customer,
					AllSupplier =
						DataSource.Suppliers.Where(supplier => supplier.Country == customer.Country && supplier.City == customer.City)
				});

			foreach (var item in suppliersForCustomerWithoutGroupBy)
			{
				ObjectDumper.Write(item.NewCustomer);
				ObjectDumper.Write(item.AllSupplier);
				ObjectDumper.Write("---------------");
			}

			ObjectDumper.Write("--------------------------------------------------------------------------------");

			var groupedSupplier = DataSource.Suppliers.GroupBy(supplier => new { supplier.City, supplier.Country });

			var suppliersForCustomerWithGroupBy = DataSource.Customers
				.Select(customer => new
				{
					NewCustomer = customer,
					AllSupplier = groupedSupplier
						.Where(supplier => supplier.Key.City.Equals(customer.City) && supplier.Key.Country.Equals(customer.Country))
				});

			foreach (var item in suppliersForCustomerWithGroupBy)
			{
				ObjectDumper.Write(item.NewCustomer);
				foreach (var supplier in item.AllSupplier)
				{
					ObjectDumper.Write(supplier);
				}

				ObjectDumper.Write("---------------");
			}
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 3")]
		[Description("Найдите всех клиентов, у которых были заказы, превосходящие по сумме величину X")]
		public void Linq3()
		{
			decimal price = 934;

			var richCustomers = DataSource
				.Customers
				.Where(customer => customer.Orders.Any(o => o.Total > price));
			
			foreach (var customer in richCustomers)
			{
				ObjectDumper.Write(customer);
			}
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 4")]
		[Description(
			"Выдайте список клиентов с указанием, начиная с какого месяца какого года они стали клиентами (принять за таковые месяц и год самого первого заказа)")]
		public void Linq4()
		{
			var customers = DataSource.Customers.Where(customer => customer.Orders.Any())
				.Select(customer => new {customer.CompanyName, DataBeginning = customer.Orders.Min(order => order.OrderDate)});

			foreach (var newCustomer in customers)
			{
				ObjectDumper.Write(newCustomer);
			}
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 5")]
		[Description(
			"Сделайте предыдущее задание, но выдайте список отсортированным по году, месяцу, оборотам клиента (от максимального к минимальному) и имени клиента")]
		public void Linq5()
		{
			var customers = DataSource.Customers.Where(customer => customer.Orders.Any())
				.Select(customer => new {customer.CompanyName, DataBeginning = customer.Orders.Min(order => order.OrderDate), customer.Orders})
				.OrderByDescending(newCustomer => newCustomer.DataBeginning.Year)
				.ThenByDescending(newCustomer => newCustomer.DataBeginning.Month)
				.ThenByDescending(newCustomer => newCustomer.Orders?.Sum(order => order.Total))
				.ThenByDescending(newCustomer => newCustomer.CompanyName);

			foreach (var newCustomer in customers)
			{
				ObjectDumper.Write(newCustomer);
			}
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 6")]
		[Description(
			"Укажите всех клиентов, у которых указан нецифровой почтовый код или не заполнен регион или в телефоне не указан код оператора (для простоты считаем, что это равнозначно «нет круглых скобочек в начале»).")]
		public void Linq6()
		{
			var customers = DataSource.Customers.Where(customer =>
				customer.PostalCode?.All(char.IsNumber) != true ||
				string.IsNullOrWhiteSpace(customer.Region) || customer.Phone.StartsWith("("));

			foreach (var newCustomer in customers)
			{
				ObjectDumper.Write(newCustomer);
			}
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 7")]
		[Description(
			"Сгруппируйте все продукты по категориям, внутри – по наличию на складе, внутри последней группы отсортируйте по стоимости")]
		public void Linq7()
		{
			
			var products = DataSource.Products.GroupBy(product => product.Category)
				.Select(x => new
				{
					Category = x.Key,
					Availability = x.GroupBy(prod => prod.UnitsInStock != 0)
						.Select(prod => new { prod.Key, Data = prod.OrderBy(z => z.UnitPrice) })
				});


			foreach (var product in products)
			{
				ObjectDumper.Write(product.Category);
				if (product.Availability.Any())
				{
					foreach (var val in product.Availability)
					{
						ObjectDumper.Write(val.Key);
						foreach (var v in val.Data)
						{
							ObjectDumper.Write(v);
						}
					}
				}
			}
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 8")]
		[Description(
			"Сгруппируйте товары по группам «дешевые», «средняя цена», «дорогие». Границы каждой группы задайте сами")]
		public void Linq8()
		{
			decimal[] rangeMediumPrice = {20, 40};

			var products = DataSource.Products
				.GroupBy(x => DetermineProductByGold(x, rangeMediumPrice));


			foreach (var product in products)
			{
				ObjectDumper.Write(product.Key);
				foreach (var prod in product)
				{
					ObjectDumper.Write(prod);
				}

				ObjectDumper.Write("---------------");
			}
		}

		private string DetermineProductByGold(Product product, decimal[] rangeMediumPrice)
		{
			if (rangeMediumPrice.Count() != 2)
			{
				throw new ArgumentException();
			}

			if (product.UnitPrice >= rangeMediumPrice.First() && product.UnitPrice <= rangeMediumPrice.Last())
			{
				return "MediumPrice";
			}

			return product.UnitPrice > rangeMediumPrice.Last() ? "ExpensivePrice" : "CheapPrice";
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 9")]
		[Description(
			"Рассчитайте среднюю прибыльность каждого города (среднюю сумму заказа по всем клиентам из данного города) и среднюю интенсивность (среднее количество заказов, приходящееся на клиента из каждого города)")]
		public void Linq9()
		{
			var citiesAD = DataSource.Customers
				.GroupBy(c => new { c.City, c.Country })
				.Select(g => new
				{
					City = g.Key,
					Country = g.Key,
					AverageProfitability = g.SelectMany(c => c.Orders).Average(o => o.Total),
					AverageIntensity = g.Average(o => o.Orders.Length)
				});

			var cities = DataSource.Customers.Select(x => x.City).Distinct()
				.Select(x => new
				{
					City = x,
					AverageProfitability = DataSource.Customers.Where(c => c.City == x)
						.Select(o => o.Orders.DefaultIfEmpty(new Order()).Average(q => q.Total)),
					AverageIntensity = DataSource.Customers.Where(c => c.City == x).Average(o => o.Orders.Count())
				});

			foreach (var city in cities)
			{
				ObjectDumper.Write(city.City);
				ObjectDumper.Write(city.AverageProfitability);
				ObjectDumper.Write(city.AverageIntensity);
				ObjectDumper.Write("---------------");
			}
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 10")]
		[Description(
			"Сделайте среднегодовую статистику активности клиентов по месяцам (без учета года), статистику по годам, по годам и месяцам (т.е. когда один месяц в разные годы имеет своё значение).")]
		public void Linq10()
		{
			var allOrders = DataSource.Customers.Select(x =>
					x.Orders.Select(y => new {x.CustomerID, y.OrderDate, y.Total}))
				.SelectMany(x => x).ToList();

			var statisticsForYear = allOrders.GroupBy(x => x.OrderDate.Year).Select(x =>
				new
				{
					Year = x.Key,
					CusomersCount = x.Select(y => y.CustomerID).Distinct().Count(),
					ReceivedMoney = x.Sum(y => y.Total)
				});

			var statisticsForMonth = allOrders.GroupBy(x => x.OrderDate.Month).Select(x =>
				new
				{
					Year = x.Key,
					CusomersCount = x.Select(y => y.CustomerID).Distinct().Count(),
					ReceivedMoney = x.Sum(y => y.Total)
				});

			var statisticsForYearAndMonth = allOrders.GroupBy(x => new {x.OrderDate.Month, x.OrderDate.Year}).Select(x =>
				new
				{
					Year = x.Key,
					CusomersCount = x.Select(y => y.CustomerID).Distinct().Count(),
					ReceivedMoney = x.Sum(y => y.Total)
				});

			foreach (var statistic in statisticsForYearAndMonth)
			{
				ObjectDumper.Write(statistic.Year);
				ObjectDumper.Write(statistic.CusomersCount);
				ObjectDumper.Write(statistic.ReceivedMoney);
				ObjectDumper.Write("------------");
			}
		}
	}
}